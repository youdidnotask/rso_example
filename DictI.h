#ifndef __DictI_h__
#define __DictI_h__

#include <Dict.h>
#include <vector>
#include <string>

namespace Dict
{

class DictServerI : virtual public DictServer
{
private:
	::std::map< ::std::string, Translations> data;

public:
	DictServerI();
    virtual ::Dict::Translations Translate(const ::std::string&,
                                           const Ice::Current&);
};

}

#endif
