module Dict {

	sequence<string> Translations;

	exception NoTranslationException{};

	interface DictServer {
		Translations Translate(string word) throws NoTranslationException;
	};
};
