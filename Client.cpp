#include <Ice/Ice.h>
#include <IceUtil/UUID.h>
#include <Dict.h>
#include <DictI.h>
#include <string>

using namespace std;
using namespace Dict;

class DictApp : virtual public Ice::Application {

public:
	virtual int run(int, char*[]) {
		string word;
		Translations translations;
		int i;

		Ice::ObjectPrx base = communicator()->propertyToProxy("DictServer.Proxy");
		DictServerPrx dictServerPrx = DictServerPrx::checkedCast(base);
		if(!dictServerPrx) {
			throw "Invalid proxy";
		}

		while(true) {
			try {
				cout << "Enter word: ";
				cin >> word;
				translations = dictServerPrx->Translate(word);
				for(i=0; i<translations.size(); ++i) {
					cout << translations[i] << endl;
				}
			} catch(NoTranslationException ex) {
				cout << "No translation!" << endl;
			}
		}

		return 0;
	}
};

int main(int argc, char* argv[])
{
	DictApp app;
	return app.main(argc, argv, "client.config");
}

