.PHONY: all clean
all: client server

Dict.cpp Dict.h:
	slice2cpp Dict.ice

client: Client.cpp Dict.cpp DictI.cpp
	g++ -I. Client.cpp Dict.cpp DictI.cpp -lIce -lIceUtil -lpthread -o client

server: Server.cpp Dict.cpp DictI.cpp
	g++ -I. Server.cpp Dict.cpp DictI.cpp -lIce -lIceUtil -lpthread -o server

clean:
	-rm Dict.cpp Dict.h client server
