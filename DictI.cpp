
#include <DictI.h>
#include <vector>
#include <string>

::Dict::DictServerI::DictServerI()
{	
	Translations translations_0;
	translations_0.push_back("translate");
	translations_0.push_back("explain");

	Translations translations_1;
	translations_1.push_back("say");
	translations_1.push_back("tell");
	translations_1.push_back("speak");

	data["tlumaczyc"] = translations_0;
	data["mowic"] = translations_1;
}

::Dict::Translations
Dict::DictServerI::Translate(const ::std::string& word,
                             const Ice::Current& current)
{
	Translations translations = data[word];
	if(translations.empty()) {
		::Dict::NoTranslationException ex;
		throw ex;
	}
    return translations;
}
