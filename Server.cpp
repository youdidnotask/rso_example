#include <Ice/Ice.h>
#include <IceUtil/UUID.h>
#include <Dict.h>
#include <DictI.h>

using namespace std;
using namespace Dict;

class DictApp : virtual public Ice::Application {

public:
	virtual int run(int, char*[]) {
		Ice::ObjectAdapterPtr adapter = communicator()->createObjectAdapterWithEndpoints("DictAdapter", "default -p 10000");
		
		Ice::ObjectPtr object = new DictServerI();
		Ice::Identity identity;
		identity.name = "DictServer";
		adapter->add(object, identity);

		adapter->activate();
		communicator()->waitForShutdown();
		return 0;
	}
};

int main(int argc, char* argv[])
{
	DictApp app;
	return app.main(argc, argv);
}
